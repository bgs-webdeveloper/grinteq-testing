import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
      monthLabels: [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ],
      weekdayLabels: [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ],
      daysInMonth: [ 31 , 28 , 31 , 30 , 31 , 30 , 31 , 31 , 30 , 31 , 30 , 31 ],
      arrTimeInterval: ['10 AM', '11 AM', '5 PM', '8 PM'],

      selectedDayInMonth: 0,
      selectedMonth: 0,
      selectedYear: 0,

      dayNow: 0,
      monthNow: 0,

      selectedSpacingSetTime: [],
      listTimeHide: false
    }
});

export default store;