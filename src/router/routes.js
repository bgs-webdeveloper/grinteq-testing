/* import calendar from '../pages/Calendar.vue' */
import month from '../components/Month.vue'
import notFound from '../pages/NotFound.vue'

const routes = [
    { path: '/', component: month },
    { 
      path: '/:id', 
      component: month
    },
    { path: '*', component: notFound }
]

export default routes